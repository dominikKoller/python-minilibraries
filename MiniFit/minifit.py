import torch
import random
import matplotlib.pyplot as plt

def fit(model, optimizer, loss_function, training_data, validation_data, epochs, device = None):
    print('Started Training')
    training_loss = torch.zeros(epochs)
    validation_loss = torch.zeros(epochs)
    figure = plt.figure()

    for epoch in range(epochs):    
        model.train()
        running_loss = 0.0
        running_loss_count = 0
        for i, data in enumerate(training_data, 0):
        
            inputs, ground_truth = data

            if device != None:
                inputs = inputs.to(device)
                ground_truth = ground_truth.to(device)
            
            optimizer.zero_grad()
            
            outputs = model(inputs)
            loss = loss_function(outputs, ground_truth)
            loss.backward()
            optimizer.step()
            
            # print statistics
            running_loss += loss.item()
            running_loss_count += 1
        print('Finished Epoch ' + str(epoch+1) + '. Training Loss: ' +  '{0:.8f}'.format(running_loss / running_loss_count), end=' ')
        training_loss[epoch] = running_loss / running_loss_count
        figure.clear()
        plt.plot(training_loss[:epoch])
        with torch.no_grad():
            model.eval()
            running_val_loss = 0
            running_val_loss_count = 0
            for i, data in enumerate(validation_data, 0):
                inputs, ground_truth = data

                if device != None:
                    inputs = inputs.to(device)
                    ground_truth = ground_truth.to(device)

                outputs = model(inputs)
                loss = loss_function(outputs, ground_truth)
                
                running_val_loss += loss.item()
                running_val_loss_count += 1
            print("Validation Loss: " + '{0:.8f}'.format(running_val_loss / running_val_loss_count))
            validation_loss[epoch] = running_val_loss / running_val_loss_count
            plt.plot(validation_loss[:epoch])
    print('Finished Training')
    
# Sequence helper functions

def unzip(zipped):
    zipped = list(zipped) # yes yes this is not the most efficient but python does not guarantee you can iterate twice..
    return ([a for a,b in zipped], [b for a,b in zipped])

# so we can treat tensor_list like a list, and element like an element of that list
# but both are tensors, where
# tensor_list.shape = (list_length, element.shape)
def append_tensor(tensor_list, element):
    if tensor_list == None:
        return element[None, :]
    else:
        return torch.cat((tensor_list, element[None, :]), dim=0)

# Standardization functions
def standardize(sequences):
    # sequences: tensor[[value]]
    # output: tensor[[value]], (tensor[mean], tensor[std])

    standardization_data = (torch.mean(sequences), torch.std(sequences))
    
    return apply_standardization_data(sequences, standardization_data), standardization_data
    
def apply_standardization_data(values, standardization_data):
    (mean, std) = standardization_data
    
    if std == 0:
        return ((values - mean))
    else:
        return ((values - mean) / std)
    
def undo_standardization_data(standardized, standardization_data):
    # standardized: [[value]]
    # standardization_data: ([mean], [std])
    # output: [[value]]
    (mean, std) = standardization_data
    
    return standardized * std + mean

def pick_random(tensor):
    return tensor[random.randint(0, tensor.shape[0]-1)]

class TensorsDataset(torch.utils.data.Dataset):
    
    def __init__(self, X, Y):
        self.X = X
        self.Y = Y
        
        assert self.X.shape[0] == self.Y.shape[0], "input and output tensors must have the same batch dimension!"
    
    def __len__(self):
        return self.X.shape[0]

    def __getitem__(self, idx):
        return self.X[idx], self.Y[idx]
    
def total_trainable_params(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)