# MINI SIMULATION LIBRARY

from abc import ABC, abstractmethod
import torch
import random
import paper # own
import minifit # own
from ipycanvas import hold_canvas
from time import sleep

class Agent(ABC):
    
    def __init__(self):
        self.state = None # must be a one-dimensional tensor
        self.graphic = None
        self.reset_histories()
        
        self.setup()
        
    def reset_histories(self):
        self.environment_frame_history = None
        self.state_history = None
        
    @abstractmethod
    def setup(self):
        pass
    
    @abstractmethod
    def update(self, environment_frame):
        pass
    
    def step(self, environment_frame, record_history = True):
        self.update(environment_frame)
        
        # TODO make history more efficient, allocate larger space up front
        if record_history:
            self.environment_frame_history = minifit.append_tensor(self.environment_frame_history, environment_frame)
            self.state_history = minifit.append_tensor(self.state_history, self.state)
    
    def draw(self, canvas):
        if (self.graphic != None):
            self.graphic.draw(canvas)
        
class TrainedAgent(Agent):
    
    def __init__(self, learning_agent_model, input_standardization_data, output_standardization_data):
        super().__init__()
        
        self.model = learning_agent_model
        self.input_standardization_data = input_standardization_data
        self.output_standardization_data = output_standardization_data
        #self.plot_over_time = Plot_over_time()
        
    def update(self, environment_frame):
        
        # adding empty batch and sequence_length dimensions
        environment_frame = environment_frame[None, None, :]

        environment_frame_standardized = minifit.apply_standardization_data(environment_frame, self.input_standardization_data)
        with torch.no_grad():
            agent_output = self.model.forward_stateful(environment_frame_standardized)
            agent_output = minifit.undo_standardization_data(agent_output, self.output_standardization_data)
            agent_output = torch.squeeze(agent_output, 0) # remove batch dimension
            agent_output = torch.squeeze(agent_output, 0) # remove sequence_length dimension
            self.state = agent_output
           
        
class Environment(ABC):
    def __init__(self, agents, canvas = None):
        self.agents = agents
        self.canvas = canvas
        
        if canvas != None:
            canvas.on_mouse_down(self.on_mouse_down)
        
        self.environment_frame = None # represents environment data in a given frame. must be a one-dimensional tensor
        self.graphic = None
        self.setup()
        
    @abstractmethod
    def setup(self):
        pass
    
    @abstractmethod
    def update(self):
        pass
        
    def step(self, record_history = True):
        self.update()
        for agent in self.agents:
            agent.step(self.environment_frame, record_history)
            
        # TODO maybe, record some environment history?
            
    # override to control drawing behaviour
    def draw(self):
        c = self.canvas
        with hold_canvas(c):
            paper.clear_canvas(c)
            paper.draw_axis(c)
            for agent in self.agents:
                agent.draw(c)
            if self.graphic != None:
                self.graphic.draw(c)
                
    def calculate(self, steps, record_history = True, reset_histories = True):
        if reset_histories:
            for agent in self.agents:
                agent.reset_histories()
            
        for i in range(0, steps):
            self.step(record_history)
            
    # TODO allow interrupt, allow other things in parallel
    def run(self, max_steps, frame_rate = 60, record_history = True):
        if self.canvas == None:
            print("Warning, you are trying running an environment which has no canvas")
        for i in range(0, max_steps):
            self.step(record_history)
            
            if self.canvas != None:
                self.draw()
            sleep(1/frame_rate)
            
    # does not work at the moment!
    # since we're blocking the thread with sleep
    def on_mouse_down(x, y):
        pass
    
### SIMPLE EXAMPLES

class SimpleEnvironment(Environment):
    def setup(self):
        self.environment_frame = torch.zeros(1)
        self.graphic = paper.Plot_over_time()
    
    def update(self):
        if random.random() > 0.95:
            self.environment_frame = torch.rand(1) * 0.1
            
        self.graphic.update(self.environment_frame.item())
    
    #def draw(self, canvas):
    #    with hold_canvas(canvas):
    #        clear_canvas(canvas)
    #        draw_axis(canvas)
    #        canvas.save()
    #        
    #        canvas.scale(1, 0.05)
    #        for agent in self.agents:
    #            agent.draw(canvas)
    #            
    #        for agent in self.agents:
    #            canvas.translate(0, -4)
    #            agent.draw(canvas)
    #            
    #        canvas.translate(0, -1)
    #        self.influence_plot.draw(canvas)
    #        canvas.restore()
                    
# Example of a simple agent:
class SimpleAgent(Agent):
    
    def setup(self):
        self.graphic = paper.Plot_over_time()
        self.state = torch.zeros(1)
        
    def update(self, environment_frame):
        # environment_frame: tensor([1])
        self.state = self.state + environment_frame
        self.state = self.state * 0.9
        
        self.graphic.update(self.state.item())
        
class SimpleTrainedAgent(TrainedAgent):
    def setup(self):
        self.graphic = paper.Plot_over_time()
        
    def update(self, environment_frame):
        super().update(environment_frame)
        self.graphic.update(self.state.item())