## DRAWING LIBRARY

from abc import ABC, abstractmethod
from ipycanvas import Canvas, hold_canvas
import math
from time import sleep, time
from ipywidgets import Output
import torch

# class Paper:
#     def __init__(self,  

class Vector2:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        
# TODO think this through, prob represent using affine matrix
class Transform:
    def __init__(self, translate = Vector2(0,0), rotate = 0, scale = Vector2(1,1)):
        self.translate = translate
        self.rotate = rotate
        self.scale = scale
    
    def apply(self, canvas):
        canvas.translate(self.translate.x, self.translate.y)
        canvas.rotate(self.rotate)
        canvas.scale(self.scale.x, self.scale.y)

class Plot_over_time:
    def __init__(self, maxLength = 100, distance_per_frame = 0.04):
        self.history = []
        self.maxLength = maxLength
        self.distance_per_frame = distance_per_frame
        
    def update(self, value):
        self.history.insert(0, value)
        
        if(len(self.history) > self.maxLength):
            self.history.pop(-1)
        
    def draw(self, canvas):
        points = [Vector2(-i*self.distance_per_frame, v) for i,v in enumerate(self.history)]
        draw_line(canvas, points)
        

        
class Paint:
    def __init__(self, stroke = True, fill = True, stroke_style = 'black', fill_style = 'black', line_width = 0.03):
        self.stroke = stroke
        self.fill = fill
        self.stroke_style = stroke_style
        self.fill_style = fill_style
        self.line_width = line_width
        
    def apply(self, canvas):
        if(self.stroke == True):
            canvas.stroke_style = self.stroke_style
            canvas.line_width = self.line_width
            canvas.stroke()
        if(self.fill == True):
            canvas.fill_style = self.fill_style
            canvas.fill()
            
class Graphic(ABC):
    def __init__(self, paint, transform):
        self.paint = paint
        self.transform = transform
        
    @abstractmethod
    def _draw(self, canvas):
        pass
    
    def draw(self, canvas):
        canvas.save()
        self.transform.apply(canvas)
        canvas.begin_path()
        self._draw(canvas)
        self.paint.apply(canvas)
        canvas.restore()
        
class Circle(Graphic):
    def __init__(self, center = Vector2(0,0), radius = 1, paint = Paint(), transform = Transform()):
        super().__init__(paint = paint, transform = transform)
        self.center = center
        self.radius = radius
    
    def _draw(self, canvas):
        canvas.arc(self.center.x, self.center.y, self.radius, 0, 2 * math.pi)
        
class Square(Graphic):
    def __init__(self, center = Vector2(0,0), size = 1, paint = Paint(), transform = Transform()):
        super().__init__(paint = paint, transform = transform)
        self.center = center
        self.size = size
    
    def _draw(self, canvas):
        canvas.rect(self.center.x - self.size/2.0, self.center.y - self.size/2.0, self.size, self.size)
        
class Cross(Graphic):
    def __init__(self, center = Vector2(0,0), size = Vector2(1, 1), paint = Paint(), transform = Transform()):
        super().__init__(paint = paint, transform = transform)
        self.center = center
        self.size = size
    
    def _draw(self, canvas):  
        canvas.move_to(self.center.x - self.size.x / 2, self.center.y - self.size.y / 2)
        canvas.line_to(self.center.x + self.size.x / 2, self.center.y + self.size.y / 2)
        canvas.move_to(self.center.x - self.size.x / 2, self.center.y + self.size.y / 2)
        canvas.line_to(self.center.x + self.size.x / 2, self.center.y - self.size.y / 2)
        
def draw_cross(canvas, center = Vector2(0,0), size = Vector2(1, 1), paint = Paint(), transform = Transform()):
    Cross(center = center, size = size, paint = paint, transform = transform).draw(canvas)
        
def draw_circle(canvas, center = Vector2(0,0), radius = 1, paint = Paint(), transform = Transform()):
    Circle(center = center, radius = radius, paint = paint, transform = transform).draw(canvas)
    
def draw_square(canvas, center = Vector2(0,0), size = 1, paint = Paint(), transform = Transform()):
    Square(center = center, size = size, paint = paint, transform = transform).draw(canvas)
        
def draw_line(canvas, points):
    # TODO create some better universal coordinate system?
    canvas.begin_path()
    if len(points) > 0:
        canvas.move_to(points[0].x, points[0].y)
        
    for point in points[1:]:
        canvas.line_to(point.x, point.y)
    canvas.stroke()
    
def draw_axis(canvas):
    canvas.save()
    canvas.line_cap = 'square'
    canvas.begin_path()
    canvas.stroke_style = 'red'
    canvas.move_to(0,0)
    canvas.line_to(1,0)
    canvas.stroke()
    canvas.begin_path()
    canvas.stroke_style = 'green'
    canvas.move_to(0,0)
    canvas.line_to(0,1)
    canvas.stroke()
    canvas.restore()
    
def clear_canvas(canvas):
    canvas.save()

    canvas.set_transform(1, 0, 0, 1, 0, 0)
    canvas.clear_rect(0, 0, canvas.width, canvas.height)

    canvas.restore()

def set_coordinate_system(canvas):
    # vvvv-like coordinate system
    canvas.stroke_rect(0,0, canvas.width, canvas.height)
    canvas.translate(canvas.width/2, canvas.height/2)
    scale = min(canvas.width, canvas.height) / 2
    canvas.scale(scale, -scale)
    canvas.line_width = 0.03
    
# mini example for drawing

#canvas = Canvas(width=500, height=300)
#display(canvas)
#set_coordinate_system(canvas)
#plotOverTime = Plot_over_time()

#for i in range(0, 400):
#    with hold_canvas(canvas):
#        clear_canvas(canvas)
#        draw_axis(canvas)
        
#        canvas.save()
#        canvas.translate(1, 0)
#        plotOverTime.update(math.sin(i/10))
#        plotOverTime.draw(canvas)
#        canvas.restore()
#    sleep(1/60)


# HACK, generating images and getting them back to python
# warning: if you do not change anything in the image during draw, this will halt

# VERY annoying way of working with these callbacks
# TODO improve ipycanvas project to contain proper callbacks to get image data to python
# probably some frame loop, with a given framerate
# that can also run as fast as possible, but will always have image data when it's run!
# will have to be implemented in ipycanvas source though.

def generate_images(draw, images):
    out = Output()
    display(out)
    
    sample_size = images.shape[0]
    channel_size = images.shape[1]
    img_width = images.shape[2]
    img_height = images.shape[3]
    
    assert channel_size == images.shape[1]
    
    current_index = 0
        
    @out.capture()
    def on_draw_finished(value):
        nonlocal current_index 
        images[current_index] = torch.from_numpy(canvas.get_image_data()).permute(2, 0, 1) / 255.0
        print("Written image " + str(current_index) + " of " + str(sample_size-1), end="\r")
        current_index = current_index +1
    
        if current_index < sample_size:
            with hold_canvas(canvas):
                draw(canvas)
        else: 
            print("Done! Written " + str(current_index) + " images")
            # callback()
    
    canvas = Canvas(width = img_width, height = img_height, sync_image_data=True)
    canvas.observe(on_draw_finished, 'image_data')
    #display(canvas)

    with hold_canvas(canvas):
        set_coordinate_system(canvas)
        draw(canvas)